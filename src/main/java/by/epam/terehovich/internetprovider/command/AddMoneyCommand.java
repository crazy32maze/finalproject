package by.epam.terehovich.internetprovider.command;

import by.epam.terehovich.internetprovider.content.RequestContent;
import by.epam.terehovich.internetprovider.dao.CabinetDao;
import by.epam.terehovich.internetprovider.entity.User;
import by.epam.terehovich.internetprovider.exception.DAOexception;
import by.epam.terehovich.internetprovider.resource.MessageManager;
import org.apache.log4j.Logger;

/**
 * Created by aterehovich on 9/11/15.
 */
public class AddMoneyCommand implements ActionCommand{

    private final static Logger LOGGER = Logger.getLogger(AddMoneyCommand.class);

    @Override
    public String execute(RequestContent request) {
        String page = new CabinetCommand().execute(request);
        User user = (User)request.getSessionAttribute("account");
        int sum = Integer.parseInt(request.getParameter("sum"));
        if (sum>0){
            CabinetDao cabinetDao = new CabinetDao();
            int money = user.getCabinet().getMoney() + sum;
            try {
                if(cabinetDao.updateMoney(user.getId(), money)){
                    user.getCabinet().setMoney(money);
                    request.setAttribute("result", MessageManager.getProperty("message.success"));
                } else {
                    request.setAttribute("result", MessageManager.getProperty("message.error"));
                }
            } catch (DAOexception daOexception) {
                LOGGER.error(daOexception);
            }
        }
        return page;
    }
}
