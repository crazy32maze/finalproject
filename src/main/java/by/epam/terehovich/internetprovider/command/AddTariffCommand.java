package by.epam.terehovich.internetprovider.command;

import by.epam.terehovich.internetprovider.content.RequestContent;
import by.epam.terehovich.internetprovider.entity.TariffPlan;
import by.epam.terehovich.internetprovider.logic.AddTariffLogic;
import by.epam.terehovich.internetprovider.resource.MessageManager;

/**
 * Created by aterehovich on 9/6/15.
 */
public class AddTariffCommand implements ActionCommand {
    private String status;
    @Override
    public String execute(RequestContent request) {
        String page = new CabinetCommand().execute(request);
        TariffPlan tariffPlan = buildTariffPlan(request);
        if (AddTariffLogic.addTariff(tariffPlan)){
            status = MessageManager.getProperty("message.success");
            request.setAttribute("status", status);
        } else {
            status = MessageManager.getProperty("message.error");
            request.setAttribute("status", status);
        }

        new Thread(() -> {
            status = "";
        }).start();
        return page;
    }

    private TariffPlan buildTariffPlan(RequestContent request){
        String name = request.getParameter("tariff-name");
        int licenseFee = Integer.parseInt(request.getParameter("license-fee"));
        int speed = Integer.parseInt(request.getParameter("speed"));
        int traffic = Integer.parseInt(request.getParameter("traffic"));
        return new TariffPlan(0, name, licenseFee, speed, traffic);
    }
}
