package by.epam.terehovich.internetprovider.command;

import by.epam.terehovich.internetprovider.content.RequestContent;
import by.epam.terehovich.internetprovider.dao.CabinetDao;
import by.epam.terehovich.internetprovider.entity.User;
import by.epam.terehovich.internetprovider.exception.DAOexception;
import org.apache.log4j.Logger;

/**
 * Created by aterehovich on 9/9/15.
 */
public class ChangeStatusCommand implements ActionCommand {
    private final static Logger LOGGER = Logger.getLogger(ChangeStatusCommand.class);
    @Override
    public String execute(RequestContent request) {
        String page = new CabinetCommand().execute(request);
        User user = (User) request.getSessionAttribute("account");
        try {
            if (user.getCabinet().getStatus()==1){
                CabinetDao cabinetDao = new CabinetDao();
                cabinetDao.changeStatus(user.getId(), 2);
                user.getCabinet().setStatus(2);
            } else {
                CabinetDao cabinetDao = new CabinetDao();
                cabinetDao.changeStatus(user.getId(), 1);
                user.getCabinet().setStatus(1);
            }
        } catch (DAOexception e){
            LOGGER.error(e);
        }
        return page;
    }
}
