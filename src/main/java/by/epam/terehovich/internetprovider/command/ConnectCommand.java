package by.epam.terehovich.internetprovider.command;

import by.epam.terehovich.internetprovider.content.RequestContent;
import by.epam.terehovich.internetprovider.entity.Cabinet;
import by.epam.terehovich.internetprovider.entity.User;
import by.epam.terehovich.internetprovider.logic.ConnectLogic;
import by.epam.terehovich.internetprovider.resource.MessageManager;

import java.util.GregorianCalendar;

/**
 * Created by aterehovich on 9/7/15.
 */
public class ConnectCommand implements ActionCommand {
    private String result = "";

    @Override
    public String execute(RequestContent request) {
        String page = new CabinetCommand().execute(request);
        User currentUser = (User)request.getSessionAttribute("account");
        if(ConnectLogic.createCabinet(buildCabinet(request), currentUser)){
            result = MessageManager.getProperty("message.success");
        } else {
            result = MessageManager.getProperty("message.error");
        }
        request.setSessionAttribute("result", result);
        new Thread(()->{
            try {
                Thread.sleep(5000);
                result = "";
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        return page;
    }

    private Cabinet buildCabinet(RequestContent request){
        Cabinet cabinet;
        User currentUser = (User)request.getSessionAttribute("account");
        int id = currentUser.getId();
        int tariffId = Integer.parseInt(request.getParameter("tariff"));
        GregorianCalendar calendar = new GregorianCalendar();
        int money = 0;
        int status = 1;
        cabinet = new Cabinet(id, tariffId, calendar, money, status);
        return cabinet;
    }
}
