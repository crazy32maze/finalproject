package by.epam.terehovich.internetprovider.command.client;

import by.epam.terehovich.internetprovider.command.*;

/**
 * Created by aterehovich on 18.7.15.
 */
public enum CommandEnum {
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    LOCALE {
        {
            this.command = new LocaleCommand();
        }
    },
    REGISTRATION {
        {
            this.command = new RegistrationCommand();
        }
    },
    CHANGEPASS{
        {
            this.command = new ChangePasswordCommand();
        }
    },
    ADDTARIFF{
        {
            this.command = new AddTariffCommand();
        }
    },
    CONNECT{
        {
            this.command = new ConnectCommand();
        }
    },
    CHANGESTATUS{
        {
            this.command = new ChangeStatusCommand();
        }
    },
    PAYMENT{
        {
            this.command = new AddMoneyCommand();
        }
    },
    CABINET {
        {
            this.command = new CabinetCommand();
        }
    };

    ActionCommand command;
    public ActionCommand getCurrentCommand(){
        return this.command;
    }
}
