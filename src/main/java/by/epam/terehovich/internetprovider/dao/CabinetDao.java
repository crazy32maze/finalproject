package by.epam.terehovich.internetprovider.dao;

import by.epam.terehovich.internetprovider.connection.ConnectionPool;
import by.epam.terehovich.internetprovider.entity.Cabinet;
import by.epam.terehovich.internetprovider.exception.DAOexception;
import by.epam.terehovich.internetprovider.exception.PoolException;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by aterehovich on 9/7/15.
 */
public class CabinetDao extends AbstractDAO<Integer, Cabinet> {
    private static final Logger LOGGER = Logger.getLogger(CabinetDao.class);
    private static final String INSERT_CABINET = "INSERT INTO internet_provider.cabinet (idcabinet, id_account, id_tp, date_connect, money, id_status) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String FIND_BY_ID = "SELECT * FROM internet_provider.cabinet WHERE idcabinet = ?";
    private static final String UPDATE_STATUS = "UPDATE cabinet SET id_status = ? WHERE idcabinet = ?";
    private static final String UPDATE_MONEY = "UPDATE cabinet SET money = ? WHERE idcabinet = ?";
    @Override
    public List<Cabinet> findAll() throws DAOexception {
        return null;
    }

    @Override
    public Cabinet findById(Integer id) throws DAOexception {
        Cabinet cabinet = null;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(FIND_BY_ID);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(rs.getDate(4));
                cabinet = new Cabinet(rs.getInt(1), rs.getInt(3), calendar, rs.getInt(5), rs.getInt(6));
            }
        } catch (SQLException e) {
            throw new DAOexception("Can't get cabinet by id" + e);
        } finally {
            try {
                if(ps != null){
                    ps.close();
                }
                ConnectionPool.closeConnection(connection);
            } catch (PoolException e) {
                LOGGER.error("Can't close prepared Statement " + e);
            } catch (SQLException e) {
                LOGGER.error("Can't close connection" + e);
            }
        }
        return cabinet;
    }

    @Override
    public Cabinet findByKey(String key) {
        return null;
    }

    @Override
    public boolean insertNew(Cabinet entity) throws DAOexception {
        PreparedStatement ps = null;
        boolean result = false;
        try {
            ps = connection.prepareStatement(INSERT_CABINET);
            ps.setInt(1, entity.getId());
            ps.setInt(2, entity.getId());
            ps.setInt(3, entity.getIdTariff());
            ps.setDate(4, new Date(entity.getConnectDate().getTimeInMillis()));
            ps.setInt(5, entity.getMoney());
            ps.setInt(6, entity.getStatus());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOexception("Can't insert cabinet " + e);
        } finally {
            if(ps != null){
                try {
                    ps.close();
                    ConnectionPool.closeConnection(connection);
                } catch (SQLException e) {
                    LOGGER.error("Can't close prepared Statement " + e);
                } catch (PoolException e) {
                    LOGGER.error("Can't close connection" + e);
                }
            }
        }
        return true;
    }

    @Override
    public boolean deleteById(Integer id) {
        return false;
    }

    public boolean changeStatus(int id, int status) throws DAOexception {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(UPDATE_STATUS);
            ps.setInt(1, status);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOexception("Can't update status" + e);
        } finally {
            try {
                if (ps!=null){
                    ps.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Can't close prepared Statement " + e);
            }

            try {
                ConnectionPool.closeConnection(connection);
            } catch (PoolException e) {
                LOGGER.error("Can't close connection" + e);
            }
        }
        return true;
    }

    public boolean updateMoney(int id, int money) throws DAOexception {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(UPDATE_MONEY);
            ps.setInt(1, money);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOexception("Can't update money" + e);
        } finally {
            try {
                if (ps!=null){
                    ps.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Can't close prepared Statement " + e);
            }

            try {
                ConnectionPool.closeConnection(connection);
            } catch (PoolException e) {
                LOGGER.error("Can't close connection" + e);
            }
        }
        return true;
    }
}
