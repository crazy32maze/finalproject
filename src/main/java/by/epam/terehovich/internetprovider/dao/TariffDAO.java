package by.epam.terehovich.internetprovider.dao;

import by.epam.terehovich.internetprovider.connection.ConnectionPool;
import by.epam.terehovich.internetprovider.entity.TariffPlan;
import by.epam.terehovich.internetprovider.exception.DAOexception;
import by.epam.terehovich.internetprovider.exception.PoolException;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aterehovich on 24.7.15.
 */
public class TariffDAO  extends AbstractDAO<Integer, TariffPlan> {
    private static final Logger LOGGER = Logger.getLogger(TariffDAO.class);
    private final static String SELECT_ALL = "SELECT * FROM  internet_provider.tariff_plan";
    private final static String SELECT_BY_ID = "SELECT * FROM internet_provider.tariff_plan WHERE id_tp = ?";
    private final static String SELECT_BY_NAME = "SELECT * FROM internet_provider.tariff_plan WHERE name = ?";
    private final static String INSERT_TARIFF = "INSERT INTO internet_provider.tariff_plan (name, license_fee, speed, traffic) VALUES (?, ?, ?, ?) ";

    @Override
    public List<TariffPlan> findAll() throws DAOexception {
        List<TariffPlan> tariffPlans = new ArrayList<>();
        Statement st = null;
        try {
            st = connection.createStatement();
            ResultSet rs = st.executeQuery(SELECT_ALL);
            while (rs.next()){
                tariffPlans.add(new TariffPlan(rs.getInt(1), rs.getString(2), rs.getInt(3),
                        rs.getInt(4), rs.getInt(5)));
            }
        } catch (SQLException e) {
            throw new DAOexception("Can't get list of tariffs" + e);
        } finally {
            if(st != null){
                try {
                    st.close();
                } catch (SQLException e) {
                    LOGGER.error("Can't close prepared Statement " + e);
                }
            }
            try {
                ConnectionPool.closeConnection(connection);
            } catch (PoolException e) {
                e.printStackTrace();
            }
        }
        return tariffPlans;
    }

    @Override
    public TariffPlan findById(Integer id) throws DAOexception {
        TariffPlan tariffPlan = null;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(SELECT_BY_ID);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                tariffPlan = new TariffPlan(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getInt(5));
            }
        } catch (SQLException e) {
            throw new DAOexception("Can't get tariff by id");
        } finally {
            if(ps != null){
                try {
                    ps.close();
                } catch (SQLException e) {
                    LOGGER.error("Can't close prepared Statement " + e);

                }
            }
            try {
                ConnectionPool.closeConnection(connection);
            } catch (PoolException e) {
                e.printStackTrace();
            }
        }
        return tariffPlan;
    }

    @Override
    public TariffPlan findByKey(String key) {
        TariffPlan tariffPlan = null;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(SELECT_BY_NAME);
            ps.setString(1, key);
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                tariffPlan = new TariffPlan(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getInt(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(ps != null){
                try {
                    ps.close();
                } catch (SQLException e) {
                    LOGGER.error("Can't close prepared Statement " + e);
                }
            }
            try {
                ConnectionPool.closeConnection(connection);
            } catch (PoolException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public boolean insertNew(TariffPlan entity) throws DAOexception {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(INSERT_TARIFF);
            ps.setString(1, entity.getName());
            ps.setInt(2, entity.getLicenseFee());
            ps.setInt(3, entity.getSpeed());
            ps.setInt(4, entity.getTraffic());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw  new DAOexception("Error with inserting new tariff" + e);
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
                LOGGER.error("Can't close prepared Statement " + e);
            }
            try {
                ConnectionPool.closeConnection(connection);
            } catch (PoolException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        return false;
    }
}
