package by.epam.terehovich.internetprovider.dao;

import by.epam.terehovich.internetprovider.connection.ConnectionPool;
import by.epam.terehovich.internetprovider.entity.Cabinet;
import by.epam.terehovich.internetprovider.entity.User;
import by.epam.terehovich.internetprovider.exception.DAOexception;
import by.epam.terehovich.internetprovider.exception.PoolException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by aterehovich on 18.7.15.
 */
public class UserDAO extends AbstractDAO<Integer, User> {
    private final static String SELECT_PASSWORD_BY_LOGIN = "SELECT user.password FROM internet_provider.user" +
                                                        " WHERE login = ?";
    private final static String INSERT_NEW_USER = "INSERT INTO internet_provider.account (login, password, email, id_role, firstname, secondname, lastname, address, city, datebirth) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private final static String SELECT_USER_BY_LOGIN = "SELECT * FROM `internet_provider`.`account` WHERE login = ?";
    private final static String SELECT_ALL = "SELECT * FROM `internet_provider`.`account`";
    private final static String UPDATE_PASS = "UPDATE account SET password= ? WHERE id_account = ?";

    private static final Logger LOGGER = Logger.getLogger(UserDAO.class);
    @Override
    public List<User> findAll() throws DAOexception {
        List<User> users = new ArrayList<>();
        Statement st = null;
        try {
            st = connection.createStatement();
            ResultSet rs = st.executeQuery(SELECT_ALL);
            while (rs.next()){
                GregorianCalendar cal = new GregorianCalendar();
                cal.setTime(rs.getDate(11));
                User user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getInt(5), rs.getString(6), rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10), cal);
                CabinetDao cabinetDao = new CabinetDao();
                Cabinet cabinet = cabinetDao.findById(user.getId());
                user.setCabinet(cabinet);
                users.add(user);
            }

        } catch (SQLException e) {
            throw new DAOexception("Error while finding all users" + e);
        } finally {
            if(st != null){
                try {
                    st.close();
                } catch (SQLException e) {
                    LOGGER.error("Can't close prepared station " + e);
                }
            }
            try {
                ConnectionPool.closeConnection(connection);
            } catch (PoolException e) {
                LOGGER.error("Close connection error" + e);
            }
        }
        return users;
    }

    @Override
    public User findById(Integer id) {
        return null;
    }

    @Override
    public User findByKey(String key) throws DAOexception {
        User user = null;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(SELECT_USER_BY_LOGIN);
            ps.setString(1, key);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                GregorianCalendar cal = new GregorianCalendar();
                cal.setTime(rs.getDate(11));
                user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getInt(5), rs.getString(6), rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10), cal);
                CabinetDao cabinetDao = new CabinetDao();
                Cabinet cabinet = cabinetDao.findById(user.getId());
                user.setCabinet(cabinet);
            }
        } catch (SQLException e) {
            throw new DAOexception("Can't find user by key "+e);
        } finally {
            if(ps != null){
                try {
                    ps.close();
                } catch (SQLException e) {
                    LOGGER.error("Can't close prepared station " + e);
                }
            }
            try {
                ConnectionPool.closeConnection(connection);
            } catch (PoolException e) {
                LOGGER.error("Close connection error" + e);
            }
        }
        return user;
    }

    @Override
    public boolean insertNew(User entity) throws DAOexception {
        PreparedStatement ps = null;
        try {
            ps = this.connection.prepareStatement(INSERT_NEW_USER);
            ps.setString(1, entity.getLogin());
            ps.setString(2, entity.getPassword());
            ps.setString(3, entity.getEmail());
            ps.setInt(4, entity.getRole());
            ps.setString(5, entity.getFirstname());
            ps.setString(6, entity.getSecondname());
            ps.setString(7, entity.getLastname());
            ps.setString(8, entity.getAddress());
            ps.setString(9, entity.getCity());
            ps.setDate(10, new Date(entity.getBirth().getTimeInMillis()));
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOexception("Can't insert user" + e);
        } finally {
            if(ps != null){
                try {
                    ps.close();
                } catch (SQLException e) {
                    LOGGER.error("Can't close prepared station " + e);
                }
            }
            try {
                ConnectionPool.closeConnection(connection);
            } catch (PoolException e) {
                LOGGER.error("Close connection error" + e);
            }
        }
        return true;
    }

    @Override
    public boolean deleteById(Integer id) {
        return false;
    }

    public boolean updatePassword(int id, String password) throws DAOexception {
        PreparedStatement ps = null;
        try {
            ps = this.connection.prepareStatement(UPDATE_PASS);
            ps.setString(1, password);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOexception("Can't update password " +e);
        } finally {
            if(ps != null){
                try {
                    ps.close();
                } catch (SQLException e) {
                    LOGGER.error("Can't close prepared station " + e);
                }
            }
            try {
                ConnectionPool.closeConnection(connection);
            } catch (PoolException e) {
                LOGGER.error("Close connection error" + e);
            }
        }
        return true;
    }
}
