package by.epam.terehovich.internetprovider.entity;

import by.epam.terehovich.internetprovider.dao.TariffDAO;
import by.epam.terehovich.internetprovider.exception.DAOexception;
import org.apache.log4j.Logger;

import java.util.GregorianCalendar;

/**
 * Created by Artyom on 13.08.2015.
 */
public class Cabinet extends Entity {
    private static final Logger LOGGER = Logger.getLogger(Cabinet.class);
    private int idTariff;
    private GregorianCalendar connectDate;
    private int money;
    private int status;

    public Cabinet(int id) {
        super(id);
    }

    public Cabinet(int id, int idTariff, GregorianCalendar connectDate, int money, int status){
        super(id);
        this.idTariff = idTariff;
        this.connectDate = connectDate;
        this.money = money;
        this.status = status;
    }

    public int getIdTariff() {
        return idTariff;
    }

    public void setIdTariff(int idTariff) {
        this.idTariff = idTariff;
    }

    public GregorianCalendar getConnectDate() {
        return connectDate;
    }

    public void setConnectDate(GregorianCalendar connectDate) {
        this.connectDate = connectDate;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public String getTariffName(){
        TariffPlan tariffPlan = null;
        TariffDAO tariffDAO = new TariffDAO();
        try {
            tariffPlan =  tariffDAO.findById(idTariff);
        } catch (DAOexception daOexception) {
            LOGGER.error(daOexception);
        }
        return tariffPlan.getName();
    }
    public String getDateToString(){
        return connectDate.get(GregorianCalendar.DAY_OF_MONTH) + "." +
                (connectDate.get(GregorianCalendar.MONTH)+1) + "." +
                connectDate.get(GregorianCalendar.YEAR);
    }
}
