package by.epam.terehovich.internetprovider.exception;

/**
 * Created by aterehovich on 9/6/15.
 */
public class DAOexception extends Exception {
    public DAOexception() {
    }

    public DAOexception(String message) {
        super(message);
    }

    public DAOexception(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOexception(Throwable cause) {
        super(cause);
    }

    public DAOexception(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
