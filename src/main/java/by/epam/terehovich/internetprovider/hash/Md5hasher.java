package by.epam.terehovich.internetprovider.hash;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by aterehovich on 9/9/15.
 */
public class Md5hasher {
    public static String md5hash(String st){
        return DigestUtils.md5Hex(st);
    }
}
