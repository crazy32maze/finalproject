package by.epam.terehovich.internetprovider.logic;

import by.epam.terehovich.internetprovider.dao.TariffDAO;
import by.epam.terehovich.internetprovider.entity.TariffPlan;
import by.epam.terehovich.internetprovider.exception.DAOexception;
import org.apache.log4j.Logger;

/**
 * Created by aterehovich on 9/6/15.
 */
public class AddTariffLogic {
    private static final Logger LOGGER = Logger.getLogger(AddTariffLogic.class);
    public static boolean addTariff(TariffPlan tariffPlan){
        TariffDAO tariffDAO = new TariffDAO();
        try {
            tariffDAO.insertNew(tariffPlan);
            return true;
        } catch (DAOexception daOexception) {
            LOGGER.error(daOexception);
        }
        return false;
    }
}
