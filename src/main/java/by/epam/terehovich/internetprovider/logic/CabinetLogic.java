package by.epam.terehovich.internetprovider.logic;

import by.epam.terehovich.internetprovider.dao.UserDAO;
import by.epam.terehovich.internetprovider.entity.Cabinet;
import by.epam.terehovich.internetprovider.entity.User;
import by.epam.terehovich.internetprovider.exception.DAOexception;
import org.apache.log4j.Logger;

/**
 * Created by aterehovich on 22.7.15.
 */
public class CabinetLogic {
    private final static Logger LOGGER = Logger.getLogger(Cabinet.class);
    public static User getUser(String login){
        UserDAO userDAO = new UserDAO();
        User user = null;
        try {
            user = userDAO.findByKey(login);
        } catch (DAOexception daOexception) {
            LOGGER.error(daOexception);
        }
        return user;
    }
}
