package by.epam.terehovich.internetprovider.logic;

import by.epam.terehovich.internetprovider.dao.CabinetDao;
import by.epam.terehovich.internetprovider.entity.Cabinet;
import by.epam.terehovich.internetprovider.entity.User;
import by.epam.terehovich.internetprovider.exception.DAOexception;
import org.apache.log4j.Logger;

/**
 * Created by aterehovich on 9/7/15.
 */
public class ConnectLogic {
    private final static Logger LOGGER = Logger.getLogger(ConnectLogic.class);
    public static boolean createCabinet(Cabinet cabinet, User user){
        CabinetDao cabinetDao = new CabinetDao();
        try {
            if (cabinetDao.insertNew(cabinet)){
                user.setCabinet(cabinet);
                return true;
            }
        } catch (DAOexception daOexception) {
            daOexception.printStackTrace();
        }
        return false;
    }
}
