package by.epam.terehovich.internetprovider.logic;

import by.epam.terehovich.internetprovider.dao.UserDAO;
import by.epam.terehovich.internetprovider.entity.User;
import by.epam.terehovich.internetprovider.exception.DAOexception;
import by.epam.terehovich.internetprovider.hash.Md5hasher;
import org.apache.log4j.Logger;

/**
 * Created by aterehovich on 18.7.15.
 */
public class LoginLogic {
    private final static Logger LOGGER = Logger.getLogger(LoginLogic.class);
    public static boolean checkLogin(String enterLogin, String enterPassword) {
        UserDAO userDAO = new UserDAO();
        boolean result = false;
        User user;

        try {
            user = userDAO.findByKey(enterLogin);
            if (user != null) {
                result = (user.getPassword().equals(enterPassword));
            } else {
                result = false;
            }
        } catch (DAOexception daOexception) {
            LOGGER.error(daOexception);
        }
        return result;
    }
}
