package by.epam.terehovich.internetprovider.logic;

import by.epam.terehovich.internetprovider.dao.UserDAO;
import by.epam.terehovich.internetprovider.entity.User;
import by.epam.terehovich.internetprovider.exception.DAOexception;
import org.apache.log4j.Logger;

/**
 * Created by aterehovich on 20.7.15.
 */
public class RegistrationLogic {

    private static String errorMessage;
    private static final Logger LOGGER = Logger.getLogger(RegistrationLogic.class);

    public static boolean checkRegistration(User user) {
        boolean result = false;
        UserDAO userDAO = new UserDAO();
        try {
            User getUser = userDAO.findByKey(user.getLogin());
            if (getUser == null) {
                userDAO = new UserDAO();
                userDAO.insertNew(user);
                result = true;
            } else {
                errorMessage = "Login is exist";
                result = false;
            }
        } catch (DAOexception daOexception) {
            LOGGER.error(daOexception);
        }
        return result;
    }
}
