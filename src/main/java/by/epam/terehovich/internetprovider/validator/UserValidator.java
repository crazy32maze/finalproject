package by.epam.terehovich.internetprovider.validator;

import by.epam.terehovich.internetprovider.entity.User;
import by.epam.terehovich.internetprovider.hash.Md5hasher;
import by.epam.terehovich.internetprovider.resource.MessageManager;

import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by aterehovich on 9/9/15.
 */
public class UserValidator {
    private static final String LOGIN_REGEX = "[A-Za-z_0-9]{6,}";
    private static final String PASSWORD_REGEX = "[\\wА-ЯЁа-яё]{8,}";
    private static final String NAME_REGEX = "[\\wА-ЯЁа-яё]{8,}";
    private static final String ADDRESS_REGEX= "";
    private static final String CITY_REGEX="";
    private String errorMessage;
    public boolean validate(User user){
        Pattern pattern;
        Matcher matcher;
        if(!user.getLogin().isEmpty()){
            pattern = Pattern.compile(LOGIN_REGEX);
            matcher = pattern.matcher(user.getLogin());
            if(!matcher.matches()){
                errorMessage = MessageManager.getProperty("message.registration.help.login");
                return false;
            }
        }
        if(!user.getPassword().isEmpty()){
            pattern = Pattern.compile(PASSWORD_REGEX);
            matcher = pattern.matcher(user.getPassword());
            if(!matcher.matches()){
                errorMessage = MessageManager.getProperty("message.registration.help.password");
                return false;
            } else {
                user.setPassword(Md5hasher.md5hash(user.getPassword()));
            }
        }
        if(!user.getFirstname().isEmpty()){
            pattern = Pattern.compile(NAME_REGEX);
            matcher = pattern.matcher(user.getFirstname());
            if(!matcher.matches()){
                errorMessage = MessageManager.getProperty("message.registration.help.firstname");
                return false;
            }
        }
        if(!user.getLastname().isEmpty()){
            pattern = Pattern.compile(NAME_REGEX);
            matcher = pattern.matcher(user.getLastname());
            if(!matcher.matches()){
                errorMessage = MessageManager.getProperty("message.registration.help.lastname");
                return false;
            }
        }
        if(!user.getSecondname().isEmpty()){
            pattern = Pattern.compile(NAME_REGEX);
            matcher = pattern.matcher(user.getSecondname());
            if(!matcher.matches()){
                errorMessage = MessageManager.getProperty("message.registration.help.secondname");
                return false;
            }
        }
        /*if(!user.getAddress().isEmpty()){
            pattern = Pattern.compile(NAME_REGEX);
            matcher = pattern.matcher(user.getAddress());
            if(!matcher.matches()){
                errorMessage = MessageManager.getProperty("message.registration.help.address");
                return false;
            }
        }*/
        if(!user.getCity().isEmpty()){
            pattern = Pattern.compile(CITY_REGEX);
            matcher = pattern.matcher(user.getCity());
            if(!matcher.matches()){
                errorMessage = MessageManager.getProperty("message.registration.help.city");
                return false;
            }
        }
        validateBirth(user.getBirth());
        return true;
    }

    private boolean validateBirth(GregorianCalendar birthDate){
        GregorianCalendar today = new GregorianCalendar();
        long diff = today.getTimeInMillis() - birthDate.getTimeInMillis();
        long years = diff/(1000*60*60*24*365);
        if(years > 18){
            return true;
        } else {
            errorMessage = MessageManager.getProperty("message.registration.help.birthdate");
            return false;
        }
    }

    public String getErrorMessage(){
        return errorMessage;
    }
}
