<%--
  Created by IntelliJ IDEA.
  User: Artyom
  Date: 10.08.2015
  Time: 15:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="customtags" prefix="ctd" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="localization"/>
<html>
<head>
    <title><fmt:message key="page.admin-cabinet.title"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/modal.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style/style.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/tabs/tabs.css" type="text/css">
</head>
<body>
<c:import url="${pageContext.request.contextPath}/jsp/patterns/header.jsp"/>
<div class="tabs">
    <input id="tab1" type="radio" name="tabs" checked>
    <label for="tab1" title="Вкладка 1"><fmt:message key="page.cabinet.personal-info"/> </label>

    <input id="tab2" type="radio" name="tabs">
    <label for="tab2" title="Вкладка 2"><fmt:message key="page.admin-cabinet.users"/> </label>

    <input id="tab3" type="radio" name="tabs">
    <label for="tab3" title="Вкладка 3"><fmt:message key="page.admin-cabinet.apps"/> </label>

    <input id="tab4" type="radio" name="tabs">
    <label for="tab4" title="Вкладка 4"><fmt:message key="page.admin-cabinet.tariff.tariffs"/> </label>


    <section id="content1">
        <p>
        <table>
            <tr>
                <td>
                    <fmt:message key="page.cabinet.firstname"/>
                </td>
                <td>
                    ${sessionScope.account.firstname}
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="page.cabinet.lastname"/>
                </td>
                <td>
                    ${sessionScope.account.lastname}
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="page.cabinet.secondname"/>
                </td>
                <td>
                    ${sessionScope.account.secondname}
                </td>
            </tr>

        </table>
        <a href="#changepassword" class="openModal"><fmt:message key="page.cabinet.change-pass"/> </a>
        <aside id="changepassword" class="modal">
            <div>
                <form action="${pageContext.request.contextPath}/control" method="post" name="change-password"
                      class="form-registration">
                    <input name="command" type="hidden" value="changepass">
                    <table>
                        <tr>
                            <td><fmt:message key="page.cabinet.oldpass"/></td>
                            <td><input type="password" name="oldpass" size="15" maxlength="15"></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="page.cabinet.newpass"/></td>
                            <td><input type="password" name="newpass" size="15" maxlength="15"></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="page.cabinet.newpass2"/></td>
                            <td><input type="password" name="newpass2" size="15" maxlength="15"></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="submit" name="submit"
                                                   value="<fmt:message key="button.edit"/> "></td>
                        </tr>
                    </table>
                </form>
                <a href="#close" title="Закрыть">Закрыть</a>
            </div>
        </aside>
        ${result}
        </p>
    </section>
    <section id="content2">
        <p>
        <table border="1">
            <tr>
                <td>ID</td>
                <td><fmt:message key="page.registration.login"/></td>
                <td><fmt:message key="page.admin-cabinet.full-name"/></td>
                <td><fmt:message key="page.admin-cabinet.role"/></td>
                <td><fmt:message key="page.cabinet.city"/></td>
            </tr>
            <ctd:user-table/>
        </table>
        </p>
    </section>
    <section id="content3">
        <p>
            TODO:applications
        </p>
    </section>
    <section id="content4">
        <p>
            ${status}
        <table>
            <tr>
                <td><fmt:message key="page.admin-cabinet.tariff.tariff-name"/> </td>
                <td><fmt:message key="page.admin-cabinet.tariff.speed"/> </td>
                <td><fmt:message key="page.admin-cabinet.tariff.Traffic"/> </td>
                <td><fmt:message key="page.admin-cabinet.tariff.licence-fee"/> </td>
            </tr>

            <jsp:useBean id="allTariffs" class="by.epam.terehovich.internetprovider.dao.TariffDAO" scope="page"/>
            <c:forEach items="${allTariffs.findAll()}" var="tariff">
                <tr>
                    <td>${tariff.name}</td>
                    <td>${tariff.speed}</td>
                    <td>${tariff.traffic}</td>
                    <td>${tariff.licenseFee}</td>
                </tr>
            </c:forEach>
        </table>
        <a href="#addtarif" class="openModal"><fmt:message key="button.create"/> </a>
        <aside id="addtarif" class="modal">
            <div>
                <form action="${pageContext.request.contextPath}/control" method="post" name="addtariff"
                      class="form-registration">
                    <input name="command" type="hidden" value="addtariff">
                    <table>
                        <tr>
                            <td><fmt:message key="page.admin-cabinet.tariff.tariff-name"/></td>
                            <td><input type="text" name="tariff-name" size="15" maxlength="15"></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="page.admin-cabinet.tariff.licence-fee"/></td>
                            <td><input type="text" name="license-fee" size="15" maxlength="15"></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="page.admin-cabinet.tariff.speed"/></td>
                            <td><input type="text" name="speed" size="15" maxlength="15"></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="page.admin-cabinet.tariff.Traffic"/></td>
                            <td><input type="text" name="traffic" size="15" maxlength="15"></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="submit" name="submit"
                                                   value="<fmt:message key="button.create"/> "></td>
                        </tr>
                    </table>
                </form>
                <a href="#close" title="Закрыть">Закрыть</a>
            </div>
        </aside>
        </p>
    </section>
</div>

</body>
</html>
