<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setBundle basename="localization"/>
<%--
  Created by IntelliJ IDEA.
  User: aterehovich
  Date: 9/10/15
  Time: 6:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<style>

  .header {
    border: 1px solid #e5e5e5;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
    -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
    box-shadow: 0 1px 2px rgba(0,0,0,.05);
    margin: auto;
    width: auto;
    background: url("../../images/head.jpg") repeat-x center;
    text-align: center;
    font-family: sans-serif;
    font-style: italic;
    font-size: 60px;
    color: #e5e5e5;
  }
</style>
<div class="header" style="text-align: center;">
  <a href="${pageContext.request.contextPath}/index.jsp">MyTelecom</a>
  <br>
  <table class="head">
    <tr>
      <td><a href="${pageContext.request.contextPath}/index.jsp"><fmt:message key="label.to_main"/> </a> </td>
    </tr>
    <tr>
      <td><a href="${pageContext.request.contextPath}/jsp/tariffs.jsp"><fmt:message key="label.tariffs"/> </a> </td>
    </tr>
  </table>
</div>

