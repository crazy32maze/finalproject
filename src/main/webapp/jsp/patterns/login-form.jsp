<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setBundle basename="localization"/>
<form name="login" action="${pageContext.request.contextPath}/control" method="post">
  <input name="command" type="hidden" value="login">
  <table class="form-signin">
    <tr>
      <td>
        <fmt:message key="label.login"/>
      </td>
      <td>
        <input name="login" type="text" size="15" value="${login}" >
      </td>
    </tr>
    <tr>
      <td>
        <fmt:message key="label.password"/>
      </td>
      <td>
        <input name="password" type="password" size="15">
      </td>
    </tr>
    <tr>
      <td>
        <input name="submit" type="submit" value=<fmt:message key="button.login"/>>
      </td>
      <td>
        <a href="${pageContext.request.contextPath}/jsp/authenticate/registration.jsp"><fmt:message key="page.registration.title"/> </a>
      </td>
    </tr>
    <tr>
      <td colspan="2"><p class="error"> ${errorLoginPassMessage}</p></td>
    </tr>
  </table>
  <br>

</form>
