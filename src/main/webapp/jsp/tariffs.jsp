<%@page contentType="text/html; charset = utf-8" pageEncoding="UTF-8" session="true" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="customtags" prefix="ctd"%>

<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="localization"/>
<html>
<html>
<head>
  <title><fmt:message key="page.tariffs.title"/> </title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/modal.css" type="text/css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style/style.css" type="text/css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/tabs/tabs.css" type="text/css">
</head>
<body>
  <c:import url="${pageContext.request.contextPath}/jsp/patterns/header.jsp"/>
  <br>
  <fmt:message key="page.tariffs.welcome"/>
  <br>
  <jsp:useBean id="allTariffs" class="by.epam.terehovich.internetprovider.dao.TariffDAO"/>
  <c:forEach items="${allTariffs.findAll()}" var="tariff">
    <table class="form-signin">
      <tr>
        <td colspan="2">${tariff.name}</td>
      </tr>
      <tr>
        <td><fmt:message key="page.admin-cabinet.tariff.licence-fee"/> </td>
        <td>${tariff.licenseFee}</td>
      </tr>
      <tr>
        <td><fmt:message key="page.admin-cabinet.tariff.speed"/> </td>
        <td>${tariff.speed} </td>
      </tr>
      <c:choose>
        <c:when test="${tariff.traffic==0}">
          <tr>
            <td><fmt:message key="page.admin-cabinet.tariff.Traffic"/> </td>
            <td>&#8734;</td>
          </tr>
        </c:when>
        <c:otherwise>
          <tr>
            <td><fmt:message key="page.admin-cabinet.tariff.Traffic"/> </td>
            <td>${tariff.traffic}</td>
          </tr>
        </c:otherwise>
      </c:choose>
    </table>
  </c:forEach>
</body>
</html>
