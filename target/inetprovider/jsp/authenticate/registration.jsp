<%--
  Created by IntelliJ IDEA.
  User: aterehovich
  Date: 18.7.15
  Time: 22.27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="utf-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="customtags" prefix="ctg"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="localization"/>
<html>
<head>
    <title><fmt:message key="page.registration.title"/> </title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style/style.css" type="text/css"/>

</head>
<body>
<c:import url="${pageContext.request.contextPath}/jsp/patterns/header.jsp"/>
  <center>
    <form name="registration" action="${pageContext.request.contextPath}/control" method="post">
    <input name="command" type="hidden" value="registration">
  <table class="registration">
    <tr>
      <td>
        <label for="login"><fmt:message key="page.registration.login"/> </label>
      </td>
      <td>
          <input name="login" type="text" size="15" id="login" pattern="[A-Za-z_0-9]{6,}"
                 required="required" title="<fmt:message key="message.registration.help.login"/> ">
      </td>
    </tr>
    <tr>
      <td>
        <label for="password"><fmt:message key="page.registration.password"/></label>
      </td>
      <td>
        <input name="password" type="password" size="15" id="password" pattern="[\wА-ЯЁа-яё]{8,}"
                required="required" title="<fmt:message key="message.registration.help.password"/> ">
      </td>
    </tr>
    <tr>
      <td>
        <label for="password2"> <fmt:message key="page.registration.password_again"/></label>
      </td>
      <td>
        <input name="password2" type="password" size="15" id="password2" pattern="[\wА-ЯЁа-яё]{8,}"
                required="required" title="<fmt:message key="message.registration.help.password"/> ">
      </td>
    </tr>
    <tr>
      <td>
        <label for="email"><fmt:message key="page.registration.email"/></label>
      </td>
      <td>
        <input name="email" type="email" size="15" id="email" required="required"
                title="<fmt:message key="message.registration.help.email"/> ">
      </td>
    </tr>
    <tr>
      <td>
        <label for="firstname"><fmt:message key="page.registration.firstname"/></label>
      </td>
      <td>
        <input name="firstname" type="text" size="15" id="firstname" required="required"
               pattern="[А-ЯЁ][А-ЯЁа-яё\s.-]+|[A-Z][\w\s.-]+"
                title="<fmt:message key="message.registration.help.firstname"/> ">
      </td>
    </tr>
    <tr>
      <td>
        <label for="lastname"><fmt:message key="page.registration.lastname"/></label>
      </td>
      <td>
        <input name="lastname" type="text" size="15" id="lastname" required="required"
                pattern="[А-ЯЁ][А-ЯЁа-яё\s.-]+|[A-Z][\w\s.-]+"
                title="<fmt:message key="message.registration.help.lastname"/> ">
      </td>
    </tr>
    <tr>
      <td>
        <label for="secondname"><fmt:message key="page.registration.secondname"/>
        </label>
      </td>
      <td>
        <input name="secondname" type="text" size="15" id="secondname" required="required"
                pattern="[А-ЯЁ][А-ЯЁа-яё\s.-]+|[A-Z][\w\s.-]+"
                title="<fmt:message key="message.registration.help.secondname"/> ">
      </td>
    </tr>
    <tr>
      <td>
        <label for="address"><fmt:message key="page.registration.address"/></label>
      </td>
      <td>
        <input name="address" type="text" size="15" id="address" required="required"
                 title="<fmt:message key="message.registration.help.address"/> ">
      </td>
    </tr>
    <tr>
      <td>
        <label for="city"><fmt:message key="page.registration.city"/></label>
      </td>
      <td>
        <input name="city" type="text" size="15" id="city" required="required"
                pattern="^[\wА-ЯЁа-яё\p\-]*" title="<fmt:message key="message.registration.help.city"/> ">
      </td>
    </tr>
    <tr>
      <td>
        <label for="date"><fmt:message key="page.registration.birth"/></label>
      </td>
      <td>
        <ctg:date-chooser/>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <input name="submit" type="submit" value="<fmt:message key="button.registration"/> ">
      </td>
    </tr>
    <tr>
      <td colspan="2">
        ${errorMessage}
      </td>
    </tr>
  </table>
    </form>
  </center>
</body>
</html>
